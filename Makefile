APPLY := kubectl apply -f
DELETE := kubectl delete -f

.DEFAULT: all

all: gitlab-runner

clean: clean-gitlab-runner

clean-gitlab-runner:
	-$(DELETE) gitlab-runner-deployment.yaml
	-$(DELETE) gitlab-runner-config.yaml
	-$(DELETE) gitlab-service-account.yaml
	-$(DELETE) gitlab-namespaces.yaml
	-rm gitlab-runner-config.yaml
	-rm gitlab-token

gitlab-runner: gitlab-runner-config.yaml
	$(APPLY) gitlab-namespaces.yaml
	$(APPLY) gitlab-service-account.yaml
	$(APPLY) gitlab-runner-config.yaml
	$(APPLY) gitlab-runner-deployment.yaml

gitlab-runner-config.yaml: gitlab-token
	@TOKEN="$(shell cat gitlab-token)" envsubst < gitlab-runner-config.template.yaml > gitlab-runner-config.yaml

gitlab-token:
	@docker run --rm -it \
		--env REGISTRATION_TOKEN="$(shell read -p "registration token: " token; echo $$token)" \
		--env-file gitlab-runner-registration-vars \
		--entrypoint /bin/bash gitlab/gitlab-runner:latest \
		-c 'gitlab-runner register --non-interactive && eval $$(grep token /etc/gitlab-runner/config.toml | sed "s/ //g"); echo $$token' | tail -n 1 > gitlab-token


See [this branch](https://gitlab.wikimedia.org/jnuche/kaniko-poc/-/tree/remote-cache) for an example using Kaniko to build a relatively big (>1GB) image using remote caching. For comparison, [this other branch](https://gitlab.wikimedia.org/jnuche/kaniko-poc/-/tree/no-caching) builds the same image without caching.

The difference in execution times are notable; with the caveats that only 1 CPU was allocated for builds, and that the Docker registry was located in the same cluster:
 - [Run w/ cache](https://gitlab.wikimedia.org/jnuche/kaniko-poc/-/jobs/17807)
 - [Run w/o cache](https://gitlab.wikimedia.org/jnuche/kaniko-poc/-/jobs/17804)
 
 Though not shown here, Kaniko also allows the use of a local dir to cache base images. In a setup like the one in this repo, this could be done using a volume mount.
